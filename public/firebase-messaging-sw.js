importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js')

// Initialize the Firebase app in the service worker by passing the generated config
const firebaseConfig = {
  apiKey: 'AIzaSyCOhNM1Y0PJc58eoIC8kjiyAEK4CKPyn2c',
  authDomain: 'push-notification-demo-49b85.firebaseapp.com',
  databaseURL:
    'https://push-notification-demo-49b85-default-rtdb.asia-southeast1.firebasedatabase.app',
  projectId: 'push-notification-demo-49b85',
  storageBucket: 'push-notification-demo-49b85.appspot.com',
  messagingSenderId: '358925550799',
  appId: '1:358925550799:web:5894b78e9d7b6bd46ca3ba'
}

firebase.initializeApp(firebaseConfig)

// Retrieve firebase messaging
const messaging = firebase.messaging()

messaging.onBackgroundMessage(function (payload) {
  console.log('Received background message ', payload)

  const notificationTitle = payload.data.title
  const notificationOptions = {
    body: payload.data.body,
    icon: payload.data.icon
  }

  self.registration.showNotification(notificationTitle, notificationOptions)
})
