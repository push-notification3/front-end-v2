import {
  getMessaging,
  onMessage,
  getToken,
  deleteToken,
  type MessagePayload
} from 'firebase/messaging'
import { firebaseApi } from '@/api'

const PUBLIC_VAPID_KEY = import.meta.env.VITE_FIREBASE_PUBLIC_VAPID_KEY

export const useFirebase = () => {
  const { app } = useFirebaseStore()
  const messaging = getMessaging(app)

  async function requestPermission() {
    const result = await Notification.requestPermission()
    if (result !== 'granted') {
      return Promise.reject('Vui lòng cho phép quyền thông báo và tải lại trang')
    }
    return Promise.resolve(true)
  }

  function receiveMessage(cb?: (payload: MessagePayload) => void) {
    onMessage(messaging, (payload) => {
      console.log('Message received. ', payload)
      if (cb) cb(payload)
    })
  }

  async function subscribe() {
    try {
      const token = await getToken(messaging, {
        vapidKey: PUBLIC_VAPID_KEY
      })
      const data = await firebaseApi.create({
        token: token
      })
      return Promise.resolve(data)
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async function unSubscribe() {
    try {
      await Promise.all([deleteToken(messaging), firebaseApi.delete()])
      return Promise.resolve('Đã hủy đăng ký thông báo')
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async function checkNotificationStatus() {
    try {
      await getToken(messaging, {
        vapidKey: PUBLIC_VAPID_KEY
      })
      await firebaseApi.get()
      return Promise.resolve(true)
    } catch (error) {
      return Promise.reject(error)
    }
  }

  return {
    requestPermission,
    subscribe,
    unSubscribe,
    checkNotificationStatus,
    receiveMessage
  }
}
