import { defineStore } from 'pinia'
import { initializeApp } from 'firebase/app'
import { getDatabase, set, ref, get, child, onValue } from 'firebase/database'
import firebaseConfig from '@/assets/configs/firebase.json'

export const useFirebaseStore = defineStore('firebase', () => {
  const app = initializeApp(firebaseConfig)
  const database = getDatabase(app)

  const dbQuery = () => {
    const messages = {
      create: async (data: { content: string }) => {
        try {
          await set(ref(database, 'messages/' + Date.now()), data)
          return Promise.resolve(true)
        } catch (error) {
          return Promise.reject(error)
        }
      },
      findAll: async (): Promise<FirebaseDataList<Message>> => {
        try {
          const dbRef = ref(database)
          const snapshot = await get(child(dbRef, 'messages'))
          if (snapshot.exists()) {
            console.log(snapshot.val())
            return Promise.resolve(snapshot.val())
          } else {
            return Promise.resolve({})
          }
        } catch (error) {
          return Promise.reject(error)
        }
      },
      findAllRealTime: (listData: Ref<FirebaseDataList<Message>>) => {
        const dbRef = ref(database, 'messages')
        onValue(dbRef, (snapshot) => {
          const data = snapshot.val()
          listData.value = data
        })
      }
    }
    return {
      messages
    }
  }

  return {
    app,
    database,
    dbQuery
  }
})

export interface FirebaseDataList<T = any> {
  [id: number]: T
}

export interface Message {
  content: string
}
