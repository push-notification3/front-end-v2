import { defineStore } from 'pinia'
import type { User } from '@/api'
import { authApi } from '@/api'

export const useAuthStore = defineStore('auth', () => {
  const profile = ref<User | null>(null)

  async function login(data: { username: string; password: string }) {
    try {
      const res = await authApi.login(data)
      const { accessToken } = res.data
      localStorage.setItem('accessToken', accessToken)
      return Promise.resolve(res)
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async function fetchProfile() {
    try {
      const res = await authApi.me()
      profile.value = res.data
      return Promise.resolve(res)
    } catch (error) {
      return Promise.reject(error)
    }
  }

  return {
    profile,
    login,
    fetchProfile
  }
})
