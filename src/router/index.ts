import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/Home/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/Auth/LoginView.vue')
    }
  ]
})

router.beforeEach(async (to, from, next) => {
  // -------- Authentication --------
  const authStore = useAuthStore()
  if (authStore.profile === null) {
    await authStore.fetchProfile().catch(() => {})
  }
  if (authStore.profile === null && to.name !== 'login') {
    return next({ name: 'login' })
  } else if (authStore.profile !== null && to.name === 'login') {
    return next({ name: 'home' })
  }
  next()
})

export default router
