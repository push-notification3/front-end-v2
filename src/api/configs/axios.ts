import axios from 'axios'

const http = axios.create({
  baseURL: `${import.meta.env.VITE_API_HOST}`
})

http.interceptors.request.use((config) => {
  const token = localStorage.getItem('accessToken')
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  } else {
    delete config.headers.Authorization
  }
  return config
})

export default http
