import { axios } from '../index.config'
import type { User } from './user'

export type AuthTokenResult = {
  exp: number
  accessToken: string
  refreshToken: string
}

const prefix = '/v1/auth'

export const authApi = {
  login: (data: { username: string; password: string }) =>
    axios.post<AuthTokenResult>(`${prefix}/signin`, data),
  me: () => axios.get<User>(`${prefix}/me`)
}
