import { axios } from '../index.config'

export interface FirebaseToken {
  id: number
  token: string
  createdAt: string
  updatedAt: string
}

export interface FirebaseTokenForm {
  token: string
}

export interface FirebaseTopic {
  id: number
  name: string
  key: string
  createdAt: string
  updatedAt: string
  FirebaseTopicMember: FirebaseTopicMember[]
}

export interface FirebaseTopicMember {
  id: number
  topicId: number
  tokenId: number
  createdAt: string
  updatedAt: string
}

const prefix = '/v1/firebase'

export const firebaseApi = {
  get: () => axios.get<FirebaseToken>(prefix),
  create: (data: FirebaseTokenForm) => axios.post<FirebaseToken>(prefix, data),
  update: (data: FirebaseTokenForm) => axios.patch<FirebaseToken>(prefix, data),
  delete: () => axios.delete<FirebaseToken>(prefix),
  sendToUser: (userId: number, content: string) =>
    axios.post<FirebaseToken>(`${prefix}/send-message/${userId}`, { content }),

  topic: {
    getList: () => axios.get<FirebaseTopic[]>(`${prefix}/topics`),
    join: (topicId: number) => axios.post<FirebaseTopicMember>(`${prefix}/topics/join/${topicId}`),
    leave: (topicId: number) => axios.delete<FirebaseTopic>(`${prefix}/topics/leave/${topicId}`)
  }
}
