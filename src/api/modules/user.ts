import { axios } from '../index.config'

export interface User {
  id: number
  username: string
}

const prefix = '/v1/users'

export const userApi = {
  login: (data: { username: string; password: string }) =>
    axios.post<User>(`/v1/auth/signin`, data),
  me: () => axios.get<User>(`/v1/auth/me`),
  getList: () => axios.get<User[]>(prefix)
}
